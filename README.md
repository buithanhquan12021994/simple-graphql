
# GRAPH QL (Basic knowledge) - Source code and theory will be update continously

- GraphQL is a language type for proceed and query for API. It provide for client a simple way to get the correct resource through request.

- GraphQL is developed base on 3 featured:
1. It makes all data from serveral resource will be simple.
2. Allow for client to define a correct data which they need.
3. It used to use a "type system" for declaration data.

## Why REST can be replaced by GraphQL

- Rest is experiencing problems because its data responses are returning too many or too few. This problem makes a serious affect to performance of applications. Therefore, it is necessary to use GraphQL to replace Rest.

## TYPE

**We have 3 root types. Includes:**
1. Query
2. Mutation
3. Subscription

**Common Type**
1. Int
2. Float
3. String
4. Boolean
5. ID


### 1. Query
- The place that client can define what they need when call the API from GraphQL server.
~~~~
// clients

query { 
  searchUser(onDeleted: true, name: 'name-user', email: 'email-user' ) {
    name
    email
    role
  }
}
~~~~


### 2. Mutation feature
- GraphQL has the ability to send queries and is called mutations. There are 3 types: **Update**, **Create**, **DELETE**. You should declare mutation in **typeDefs**, it will start with a prefix name


**Define in typeDefs**
```
type Mutation = {
  updateProduct(id: ID!, input: updateProductInput): Product!
}

type Product {
  id: ID!
  name: String!
  description: String!
  quantity: Int!
  image: String!
  price: Float!
  onSale: Boolean!
  rating: Int
  categoryId: String
  category: Category
  reviews: [Review!]!
}
```

**Define in resolves**
~~~~
  updateProduct: (parent, args, context) => {
    /*
    *
    * Do some logic in here
    *
    */

    // should return Product object -> because we define in Mutation that return a Product TYPE
    return instanceProduct; //
  },
~~~~


**Mutation from client**

~~~~
mutation {
  updateProduct(id: "53a0724c-a416-4cac-ae45-bfaedce1f147", input: {
    "name": "update name",
    "description": " update description",
    "quantity": 1,
    "price": 11.11,
    "image": "img-1",
    "onSale": true,
    "categoryId": "c01b1ff4-f894-4ef2-b27a-22aacc2fca70"
  }) {
    id
    description
    quantity
    price
    image
    onSale
    categoryId
    category {
      id 
      name
    }
  }
}
~~~~
## **Define Schema and Type system**
- In GraphQL system, you should define the schema for the api. All of types are listed in 1 API should be defined in schema that is used **GraphQL Schema Definition Language (SDL)**

- These Schemas are used such as a contract between client and server to declare how the client can access the data.

- You should define a schema and type in typeDefs first, after that you should define functions inside resolves.

~~~~
// in schema.js

const { gql  } = require('apollo-server');
const typeDefs = gql`
  type Query {
    products: [Product]!
  }

  type Product {
    id: ID!
    name: String!
    description: String!
    quantity: Int!
    image: String!
    price: Float!
    onSale: Boolean!
    rating: Int
    categoryId: String
    category: Category
    reviews: [Review!]!
  }

  type Category {
    id: ID!
    name: String!
  }

   type Review {
    id: ID!
    date: String!
    title: String!
    comment: String!
    rating: Int!
    productId: String!
  }
`

// in resolvers.js

const resolvers = {
  Query: {
    products: (parent, args, context) => context.db.products;
  },
  Product: { 
    category: (parent, args, context) => context.db.categories.find(item => item.id == parent.categoryId);
    reviews: (parent, args, context) => context.db.reviews.filter(item => item.productId === parent.id);
  }
}
~~~~


**Call query from client**

~~~~
query { 
  products {
    id
    name
    description
    image
    price
    onSale
    rating
    category: {
      id 
      name
    }
    reviews {
      id
      title
      comment
    }
  }
}
~~~~

### 3. Subscription
- Such as a realtime, all system need a realtime to handle event immediately. GraphQl provides a solution as realtime that can be call a subscriptions. Once the client subscribe a event, it will start and keep connecting with server. Anytime event is fired, server will push the data to the client who subscribe that event. It is not such as **Mutation** or **Query**, it like **request - response - cycle**

~~~~
// syntax for client

subscriptions { 
  newUser {
    name
  }
}
~~~~


# How can run project

## Requirement
- node
- apollo server

## Install library
`npm install`

## Run server
`npm run start` (server running on localhost and port: 4000)

## How to write the API
1. Define Schema in typeDefs
2. Define a function inside Resolvers
3. Check a query in host: [Host](http://localhost:4000)
4. If you want to share a function or reuse them everywhere you want, just put them to init server (index.js)
~~~~
// use context 
// index.js
const { ApolloServer, gql  } = require('apollo-server');

const apploServer = new ApolloServer({
  typeDefs: gql`
    // define type and Schema for API
  `,
  resolvers: {
    // define a function for typeDefs to excute a api above
  },
  context: {
    shareContextParamsInResolve: () => { console.log('share function') },
    ...
  }
});
~~~~

