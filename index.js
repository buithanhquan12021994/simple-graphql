const { ApolloServer, gql  } = require('apollo-server');
const { db } = require('./db.js')
const { typeDefs } = require('./schema');
const { Query, Category, Product, Mutation } = require('./resolvers/index.js');

// should define in apollo-server graphQL, define how our query is going to look


// reolve typeDef above
// Resolvers are actual functions that return the data that confirm to the way that we have specified them in the schema
const resolvers = {
  // resolver for only Query Type
  ...Query,
  // resolver for Category
  ...Category,
  // resolver for Product
  ...Product,
  // mutation for creation, deletion, updation
  ...Mutation
}

const apploServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: {
    shareContextParamsInResolve: () => { console.log(this) },
    db
  }
});

// initialize server
apploServer.listen().then(({url}) => {
  console.log(`Server is running with url: ${url}`)
});