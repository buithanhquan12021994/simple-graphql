exports.Category = {
  products: (parent, args, { db }) => {
    // get id from nested object with relationship
    return db.products.filter(item => item.categoryId == parent.id);
  },
  total: (parent, args, { db }) => {
    return db.products.filter(item => item.categoryId == parent.id).length;
  },
  searchProducts: (parent, args, context) => {
    let { products: filteredProduct, reviews: reviewsFilter } = context.db;
    const { filter } = args;
    if (filter) {
      let {onSale, avgRating}  = filter;
      if (onSale) {
        filteredProduct = filteredProduct.filter(item => item.onSale);
      }
      if (avgRating && [1,2,3,4,5].includes(avgRating)) {
        filteredProduct = filteredProduct.filter(item => {
          let avg = 0;
          let reviews = reviewsFilter.filter(itemReviews => itemReviews.productId == item.id);
          if (reviews.length > 0) {
            let count = reviews.reduce(function (pre, cur) { return pre + cur.rating }, 0);
            avg = count / (reviews.length);
          }
          return avg >= avgRating;
        });
      }
      return filteredProduct;
    }
    return filteredProduct;
  }
}