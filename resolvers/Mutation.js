const { v4: uuidv4 } = require('uuid');

exports.Mutation = {
  // Create new
  addCategory: (parent, args, { db }) => {
    const { input } = args;
    const { name } = input;
    const newCategory = {
      id: uuidv4(),
      name: name
    }

    db.categories.push(newCategory);

    return newCategory
  },

  addProduct: (parent, args, { db }) => {
    let { input } = args;
   
    let { name, description, quantity, image, price, onSale, categoryId } = input
    let newProduct =  {
      id: uuidv4(),
      name, 
      description,
      quantity,
      image,
      price,
      onSale,
      categoryId
    };

    // push to array
    db.products.push(newProduct);

    return newProduct;
  },

  addReview: (parent, args,  { db }) => {
    let { date, title, comment, rating, productId } = args.input;
    
    let newReview = {
      id: uuidv4(),
      date,
      title,
      comment,
      rating,
      productId
    }
    // push to array
    db.reviews.push(newReview);


    return newReview;
  },

  // Delete
  deleteCategory: (parent, args, { db }) => {
    let { id } = args;
    // find Category
    db.categories = db.categories.filter(item => item.id != id);
    db.products = db.products.map(item => {
      if (item.categoryId == id) {
        item.categoryId = null;
      }
      return item;
    })
    return true;
  },

  deleteProduct: (parent, args, { db }) => {
    let { id } = args;
    db.products = db.products.filter(item => item.id != id);
    db.reviews = db.reviews.filter(item => item.productId !== id);
    return true;
  },

  deleteReview: (parent, args, { db }) => {
    let { id } = args;
    db.reviews = db.reviews.filter(item => item.id != id); // filter all reviews with the same id
    return true;
  },

  // update 
  updateCategory: (parent, args, { db }) => {
    let { id, input } = args;
    let find = db.categories.find(item => item.id === id);
    find.name = input.name;

    return find;
  },
  updateProduct: (parent, args, { db }) => {
    let { id, input } = args;
    let findProduct = db.products.find(item => item.id === id);
    if (findProduct) {
      let { name, description, quantity, price, image, onSale, categoryId } = input;
      console.log(categoryId)
      findProduct = {
        ...findProduct,
        name,
        description,
        quantity,
        price,
        image, 
        onSale,
        categoryId: categoryId ? categoryId : findProduct.categoryId
      }
    } else {
      throw new Error('Product not found!')
    }
    return findProduct
  },

  updateReview: (parent, args, { db }) => {
    let { id, input } = args
    let index = db.reviews.findIndex(item => item.id === id);

    if (index < 0) return null

    let { date, title, comment, rating, productId }  = input
    db.reviews[index] = {
      ...db.reviews[index],
      date,
      title,
      comment,
      rating,
      productId: productId ? productId : db.reviews[index].productId
    }
    return db.reviews[index];
  }
}