exports.Query = {
  hello: () => 'should not be null',
  numberOrAnimals: () => 1,
  price: () => 2.5, 
  isAuth: () => true,
  arrayString: () => null,
  arrayStringNotNull: () => [1,2,'not null',3,'text'],

  products: (parent, args, { db }) => {
    return db.products;
  },

  product: (parent, args, { db }) => {
    const { id } = args;
    return db.products.find(item => item.id == id);
  },

  categories: (parent, args, { db }) => {
    return db.categories
  },

  category: (parent, args, { db }) => {
    return db.categories.find(item => item.id == args.id);
  },
  
  searchProducts: (parent, args, { db }) => {
    let { products: filteredProduct, reviews: reviewsFilter } = db;
    const { filter } = args;
    if (filter) {
      let {onSale, avgRating}  = filter;
      if (onSale) {
        filteredProduct = filteredProduct.filter(item => item.onSale);
      }
      if (avgRating && [1,2,3,4,5].includes(avgRating)) {
        filteredProduct = filteredProduct.filter(item => {
          let avg = 0;
          let reviews = reviewsFilter.filter(itemReviews => itemReviews.productId == item.id);
          if (reviews.length > 0) {
            let count = reviews.reduce(function (pre, cur) { return pre + cur.rating }, 0);
            avg = count / (reviews.length);
          }
          return avg >= avgRating;
        });
      }
      return filteredProduct;
    }
    return filteredProduct;
  },

  searchReviewByFilter: (parent, args, { db }) => { 
    let reviews = JSON.parse(JSON.stringify(db.reviews));
    let { filter } = args;
    console.log(filter)
    
    if (filter) {
      let { productId } = filter;
      if (typeof productId != 'undefined') {
        reviews = reviews.filter(item => item.productId == productId);
      }
    }

    return reviews;
  },

  review: (parent, args, { db }) => {
    let { id } = args;
    return db.reviews.find(item => item.id === id);
  }
}