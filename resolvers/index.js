const Query = require('./Query')
const Category = require('./Category')
const Product = require('./Product');
const Mutation = require('./Mutation')

exports.Query = Query;
exports.Category = Category;
exports.Product = Product;
exports.Mutation = Mutation;