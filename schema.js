const { gql  } = require('apollo-server');

// 1. Type defination ( the way that our data is going to look like, the way that our query is going to look at the way that our mutation is going to look)
// Simple Type:  String, Int, Float, Boolean
// can return resolve null
// Avoid null: String!
// type array not null: [String!]!
// type array can be null: [String] | null
const typeDefs = gql`
  type Query {
    hello: String!
    numberOrAnimals: Int
    price: Float
    isAuth: Boolean
    arrayString: [String]
    arrayStringNotNull: [String!]
    products: [Product!]!
    product(id: ID!): Product
    categories: [Category!]!
    category(id: ID!): Category
    searchProducts(filter: ProductsFilterInput): [Product!]!
    searchReviewByFilter(filter: ReviewsFilterInput): [ReviewFilter!]!
    review(id: ID!): Review
  }

  type Mutation {
    # Add
    addCategory(input: AddCategory): Category! 
    addProduct(input: AddProduct): Product!
    addReview(input: AddReview): Review!
    # Delete
    deleteCategory(id: ID!): Boolean!
    deleteProduct(id: ID!): Boolean!
    deleteReview(id: ID!): Boolean!

    # Update
    updateCategory(id: ID!, input: updateCategoryInput): Category!
    updateProduct(id: ID!, input: updateProductInput): Product!
    updateReview(id: ID!, input: updateReviewInput): Review
  }


  type Product {
    id: ID!
    name: String!
    description: String!
    quantity: Int!
    image: String!
    price: Float!
    onSale: Boolean!
    rating: Int
    categoryId: String
    category: Category
    reviews: [Review!]!
  }
  
  type Category {
    id: ID!
    name: String!
    products: [Product!]!
    total: Int
    searchProducts(filter: ProductsFilterInput): [Product!]!
  }

  type Review {
    id: ID!
    date: String!
    title: String!
    comment: String!
    rating: Int!
    productId: String!
  }

  type ReviewFilter {
    id: ID!
    date: String!
    title: String!
    comment: String!
    rating: Int!
    productId: ID
  }

  input ProductsFilterInput {
    onSale: Boolean
    avgRating: Int
  }

  input AddCategory {
    name: String!
  }

  input AddProduct {
    name: String!
    description: String!
    quantity: Int!
    image: String!
    price: Float!
    onSale: Boolean!
    categoryId: String!
  }

  input AddReview {
    date: String!
    title: String!
    comment: String!
    rating: Int!
    productId: ID!
  }

  input ReviewsFilterInput {
    productId: ID
  }

  input updateCategoryInput {
    name: String!
  }

  input updateProductInput {
    name: String!
    description: String!
    quantity: Int!
    price: Float!
    image: String!
    onSale: Boolean!
    categoryId: String
  }

  input updateReviewInput {
    date: String!
    title: String!
    comment: String!
    rating: Int!
    productId: ID
  }
`;

exports.typeDefs = typeDefs